package com.prodyna.ykesen.confy.model.mapper;

import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.Talk;
import com.prodyna.ykesen.confy.model.view.EventDTO;
import com.prodyna.ykesen.confy.model.view.TalkDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.temporal.TemporalAccessor;
import java.util.HashSet;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class TalkMapper {

    @Autowired
    private TopicMapper topicMapper;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ObjectMapper objectMapper;

    public Set<TalkDTO> mapIterableToDTO(Iterable<Talk> iterable ) {

        Set<TalkDTO> set = new HashSet<>();
        iterable.forEach(entity ->set.add( mapToDto(entity)));
        return set;

    }

    public TalkDTO mapToDto (Talk entity){
        return TalkDTO.builder()
                .id(entity.getId())
                .startDateTime(entity.getStartDateTime())
                .endDateTime(entity.getEndDateTime())
                .name(entity.getName())
                .language(entity.getLanguage())
                .level(entity.getLevel())
                .room(objectMapper.mapToDto(entity.getRoom()))
                .topics(topicMapper.mapIterableToDTO(entity.getTopics()))
                .persons(personMapper.mapIterableToDTO(entity.getPersons()))
                .build();
    }



}