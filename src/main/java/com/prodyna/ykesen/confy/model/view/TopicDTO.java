package com.prodyna.ykesen.confy.model.view;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.prodyna.ykesen.confy.model.*;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDateTime;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class TopicDTO {

    private Long id;

    private String name;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    public Set<TopicDTO> parents;

    @ToString.Exclude
    public Set<TopicDTO> children;

}



