package com.prodyna.ykesen.confy.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Room {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @EqualsAndHashCode.Exclude
    @Relationship(type = "EXISTS_IN" , direction = Relationship.OUTGOING)
    private Location location;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Relationship(type = "HOLD_IN" , direction = Relationship.INCOMING)
    public Set<Talk> talks;


}
