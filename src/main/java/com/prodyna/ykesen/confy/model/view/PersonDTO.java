package com.prodyna.ykesen.confy.model.view;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.prodyna.ykesen.confy.model.Organization;
import com.prodyna.ykesen.confy.model.Talk;
import lombok.*;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class PersonDTO {

    private Long id;

    private String name;

    @EqualsAndHashCode.Exclude
    private OrganizationDTO organization;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<TalkDTO> talks;

}



