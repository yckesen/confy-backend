package com.prodyna.ykesen.confy.model.view;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.prodyna.ykesen.confy.model.Location;
import com.prodyna.ykesen.confy.model.Talk;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class EventDTO {

    private Long id;

    private String name;

    private LocalDate startDate;

    private LocalDate endDate;

    private LocationDTO location;

    public Set<TalkDTO> talks;

}



