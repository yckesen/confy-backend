package com.prodyna.ykesen.confy.model.mapper;

import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.Location;
import com.prodyna.ykesen.confy.model.Organization;
import com.prodyna.ykesen.confy.model.Room;
import com.prodyna.ykesen.confy.model.view.EventDTO;
import com.prodyna.ykesen.confy.model.view.LocationDTO;
import com.prodyna.ykesen.confy.model.view.OrganizationDTO;
import com.prodyna.ykesen.confy.model.view.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ObjectMapper {

    public LocationDTO mapToDto (Location eventEntity){
        return LocationDTO.builder()
                .id(eventEntity.getId())
                .name(eventEntity.getName())
                .build();
    }


    public RoomDTO mapToDto (Room entity){
        return RoomDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .location(mapToDto(entity.getLocation()))
                .build();
    }

    public OrganizationDTO mapToDto(Organization entity){
        return OrganizationDTO.builder().id(entity.getId()).name(entity.getName()).build();
    }



}


