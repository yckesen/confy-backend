package com.prodyna.ykesen.confy.model;

public enum Level {

    BEGINNER, ADVANCED, EXPERT

}
