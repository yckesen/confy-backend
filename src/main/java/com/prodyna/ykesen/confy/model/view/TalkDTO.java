package com.prodyna.ykesen.confy.model.view;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.prodyna.ykesen.confy.model.*;
import lombok.*;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter
@Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class TalkDTO {

    private Long id;

    private String name;

    private LocalDateTime startDateTime;

    private LocalDateTime endDateTime;

    private Language language;

    private Level level;

    private RoomDTO room;

    @ToString.Exclude
    private Set<PersonDTO> persons;

    @ToString.Exclude
    private Set<TopicDTO> topics;


}



