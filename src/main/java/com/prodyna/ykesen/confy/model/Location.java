package com.prodyna.ykesen.confy.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Location {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ToString.Exclude
    @Relationship(type = "EXISTS_IN" , direction = Relationship.INCOMING)
    public Set<Room> rooms;

    @ToString.Exclude
    @Relationship(type = "TAKES_PLACE_IN" , direction = Relationship.INCOMING)
    public Set<Event> events;


}
