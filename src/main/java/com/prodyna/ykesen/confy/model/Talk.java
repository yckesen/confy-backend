package com.prodyna.ykesen.confy.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Talk {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDateTime startDateTime;

    private LocalDateTime endDateTime;

    private Language language;

    private Level level;

    @Relationship(type = "PART_OF" , direction = Relationship.OUTGOING)
    private Event event;

    @Relationship(type = "HOLD_IN" , direction = Relationship.OUTGOING)
    private Room room;

    @ToString.Exclude
    @Relationship(type = "HOLD_BY", direction = Relationship.OUTGOING)
    private Set<Person> persons;

    @ToString.Exclude
    @Relationship(type = "CONTAINS", direction = Relationship.OUTGOING)
    private Set<Topic> topics;

}