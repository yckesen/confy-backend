package com.prodyna.ykesen.confy.model;

import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDate startDate;

    private LocalDate endDate;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @Relationship(type = "TAKES_PLACE_IN" , direction = Relationship.OUTGOING)
    private Location location;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Relationship(type = "PART_OF" , direction = Relationship.INCOMING)
    public Set<Talk> talks;

}
