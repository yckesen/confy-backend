package com.prodyna.ykesen.confy.model.mapper;

import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.view.EventDTO;
import com.prodyna.ykesen.confy.model.view.TalkDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class EventMapper {

    @Autowired
    private TalkMapper talkMapper;

    @Autowired
    private ObjectMapper objectMapper;


    public Set<EventDTO> mapIterableToDTO(Iterable<Event> iterable ) {

        Set<EventDTO> set = new HashSet<>();
        iterable.forEach(entity ->set.add( mapToDto(entity)));
        return set;


    }

    public EventDTO mapToDto (Event entity){
        return EventDTO.builder()
                .id(entity.getId())
                .startDate(entity.getStartDate())
                .endDate(entity.getEndDate())
                .name(entity.getName())
                .talks(talkMapper.mapIterableToDTO(entity.getTalks()))
                .location(objectMapper.mapToDto(entity.getLocation()))
                .build();
    }

}


