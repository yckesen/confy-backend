package com.prodyna.ykesen.confy.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Topic {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Relationship(type = "CONTAINS", direction = Relationship.INCOMING)
    public Set<Talk> talks;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Relationship(type = "IS_CHILD_OF", direction = Relationship.OUTGOING)
    public Set<Topic> parents;

    @ToString.Exclude
    @Relationship(type = "IS_CHILD_OF", direction = Relationship.INCOMING)
    public Set<Topic> children;


}



