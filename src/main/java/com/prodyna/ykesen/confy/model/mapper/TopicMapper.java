package com.prodyna.ykesen.confy.model.mapper;

import com.prodyna.ykesen.confy.model.Topic;
import com.prodyna.ykesen.confy.model.view.TalkDTO;
import com.prodyna.ykesen.confy.model.view.TopicDTO;
import org.springframework.stereotype.Component;
import java.util.Set;

import java.util.HashSet;

@Component
public class TopicMapper {

    public Set<TopicDTO> mapIterableToDTO(Iterable<Topic> iterable) {

        Set<TopicDTO> set = new HashSet<>();
        iterable.forEach(entity ->set.add( mapToDto(entity,true)));
        return set;

    }

    public TopicDTO mapToDto (Topic entity, boolean loadRecursive){
        return TopicDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .build();
    }

}