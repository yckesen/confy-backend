package com.prodyna.ykesen.confy.model;

public enum Language {

    DE, EN, FR, IT

}
