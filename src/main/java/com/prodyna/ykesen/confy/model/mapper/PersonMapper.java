package com.prodyna.ykesen.confy.model.mapper;

import com.prodyna.ykesen.confy.model.Organization;
import com.prodyna.ykesen.confy.model.Person;
import com.prodyna.ykesen.confy.model.view.OrganizationDTO;
import com.prodyna.ykesen.confy.model.view.PersonDTO;
import com.prodyna.ykesen.confy.model.view.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class PersonMapper {

    @Autowired
    private ObjectMapper objectMapper;

    public Set<PersonDTO> mapIterableToDTO(Iterable<Person> iterable ) {

        Set<PersonDTO> set = new HashSet<>();
        iterable.forEach(entity -> set.add( mapToDto(entity)));
        return set;

    }

    public PersonDTO mapToDto (Person entity){
        return PersonDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .organization(objectMapper.mapToDto(entity.getOrganization()))
                .build();
    }



}