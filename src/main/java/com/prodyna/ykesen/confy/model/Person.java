package com.prodyna.ykesen.confy.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Getter@Setter
@NodeEntity
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @EqualsAndHashCode.Exclude
    @Relationship(type = "WORKS_IN", direction = Relationship.OUTGOING)
    private Organization organization;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Relationship(type = "HOLD_BY", direction = Relationship.INCOMING)
    private Set<Talk> talks;

}