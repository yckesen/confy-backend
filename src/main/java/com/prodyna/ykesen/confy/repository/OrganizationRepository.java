package com.prodyna.ykesen.confy.repository;


import com.prodyna.ykesen.confy.model.Location;
import com.prodyna.ykesen.confy.model.Organization;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "organizations", path = "organizations")
public interface OrganizationRepository extends Neo4jRepository<Organization, Long> {

  List<Organization> findByName(@Param("name") String name);

}