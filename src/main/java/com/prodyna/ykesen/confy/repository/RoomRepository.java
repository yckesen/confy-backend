package com.prodyna.ykesen.confy.repository;


import com.prodyna.ykesen.confy.model.Person;
import com.prodyna.ykesen.confy.model.Room;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "rooms", path = "rooms")
public interface RoomRepository extends Neo4jRepository<Room, Long> {

  List<Room> findByName(@Param("name") String name);

  Room findByTalksId(@Param("id") Long id);

}