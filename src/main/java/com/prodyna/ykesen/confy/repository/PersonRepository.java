package com.prodyna.ykesen.confy.repository;


import com.prodyna.ykesen.confy.model.Organization;
import com.prodyna.ykesen.confy.model.Person;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "persons", path = "persons")
public interface PersonRepository extends Neo4jRepository<Person, Long> {

  List<Person> findByName(@Param("name") String name);

  List<Person> findByTalksId(@Param("id") Long id);

}