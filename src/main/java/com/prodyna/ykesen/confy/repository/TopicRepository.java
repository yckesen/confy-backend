package com.prodyna.ykesen.confy.repository;


import com.prodyna.ykesen.confy.model.Topic;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "topics", path = "topics")
public interface TopicRepository extends Neo4jRepository<Topic, Long> {

  List<Topic> findByName(@Param("name") String name);

  List<Topic> findByTalksId(@Param("id") Long id);



}