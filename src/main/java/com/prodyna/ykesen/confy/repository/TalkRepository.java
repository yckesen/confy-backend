package com.prodyna.ykesen.confy.repository;


import com.prodyna.ykesen.confy.model.Talk;
import com.prodyna.ykesen.confy.model.Topic;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "talks", path = "talks")
public interface TalkRepository extends Neo4jRepository<Talk, Long> {

  List<Talk> findByName(@Param("name") String name);


}