package com.prodyna.ykesen.confy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication
@EnableNeo4jRepositories
public class ConfyServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfyServiceApplication.class, args);
    }

}
