package com.prodyna.ykesen.confy.controller;

import com.google.common.collect.Lists;
import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.Talk;
import com.prodyna.ykesen.confy.repository.EventRepository;
import com.prodyna.ykesen.confy.repository.TalkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TalkContoller {

    @Autowired
    private TalkRepository talkRepository;

  // @GetMapping("/events")
   //    public String getAll(){
   //     return "Hello World";
    //   }

    @GetMapping("/talks")
    public List<Talk> getAll(){
        Iterable<Talk> talks = talkRepository.findAll();
        return Lists.newArrayList(talks);
    }
}
