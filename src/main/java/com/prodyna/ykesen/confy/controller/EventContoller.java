package com.prodyna.ykesen.confy.controller;

import com.google.common.collect.Lists;
import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.view.EventDTO;
import com.prodyna.ykesen.confy.repository.EventRepository;
import com.prodyna.ykesen.confy.service.EventViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path = "/api")
public class EventContoller {

    @Autowired
    private EventViewService eventViewService;


    @GetMapping("/events")
    public List<EventDTO> getAll(){
        List<EventDTO> result = new ArrayList<>();
        eventViewService.getAllEventsView().forEach(result::add);
        return  result;
    }
}
