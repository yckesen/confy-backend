package com.prodyna.ykesen.confy.service;

import com.prodyna.ykesen.confy.model.*;
import com.prodyna.ykesen.confy.repository.*;
import lombok.extern.java.Log;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Log
@Component
class Neo4jInitializer implements InitializingBean {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private TalkRepository talkRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private EventRepository eventRepository;


    private void dropAll(){

        topicRepository.deleteAll();
        talkRepository.deleteAll();
        roomRepository.deleteAll();
        personRepository.deleteAll();
        organizationRepository.deleteAll();
        locationRepository.deleteAll();
        eventRepository.deleteAll();

    }

    private void create(){

        Organization prodyna = Organization.builder().name("prodyna").build();
        Organization dell  = Organization.builder().name("dell").build();
        Organization redHat = Organization.builder().name("redHat").build();
        organizationRepository.saveAll(List.of(prodyna,dell,redHat));

        Person jack = Person.builder().name("Jack Hammer").organization(prodyna).build();
        Person marc = Person.builder().name("Marc Tester").organization(prodyna).build();
        Person hansi = Person.builder().name("Hansi Muller").organization(dell).build();
        Person alex = Person.builder().name("Alex Gruber").organization(redHat).build();
        Person vitali = Person.builder().name("Vitali Macher").organization(redHat).build();
        personRepository.saveAll(List.of(jack,marc,hansi,alex,vitali));

        Location vic = Location.builder().name("Vienna International Center").build();
        Location radissonHotel = Location.builder().name("Radisson Hotel").build();
        Location zurichConferenceCenter = Location.builder().name("Zurich Conference Center").build();
        locationRepository.saveAll(List.of(vic,zurichConferenceCenter,radissonHotel));

        Room vic1 = Room.builder().name("vic room 1").location(vic).build();
        Room vic2 = Room.builder().name("vic room 2").location(vic).build();
        Room vic3 = Room.builder().name("vic room 3").location(vic).build();
        Room rad1 = Room.builder().name("radisson room 1").location(radissonHotel).build();
        Room rad2 = Room.builder().name("radisson room 2").location(radissonHotel).build();
        Room zur1 = Room.builder().name("zurich room 1").location(zurichConferenceCenter).build();
        Room zur2 = Room.builder().name("zurich room 2").location(zurichConferenceCenter).build();
        roomRepository.saveAll(List.of(vic1,vic2,vic3,rad1,rad2,zur1,zur2));


        Event javaDays = Event.builder()
                .name("Java Days 2020")
                .startDate(LocalDate.of(2020,12,21))
                .endDate(LocalDate.of(2020,12,25))
                .location(vic)
        .build();

        Event kubeCon = Event.builder()
                .name("kubeCon 2021")
                .startDate(LocalDate.of(2021,1,10))
                .endDate(LocalDate.of(2021,1,15))
                .location(zurichConferenceCenter)
                .build();

        eventRepository.saveAll(List.of(javaDays,kubeCon));

        Topic lambdaExp = Topic.builder().name("Lambda Expressions").build();
        Topic funcInt = Topic.builder().name("Funtional Interfaces").build();
        Topic java = Topic.builder().name("Java").build();
        Topic microservices = Topic.builder().name("Microservices").build();
        Topic architecture = Topic.builder().name("Software Architecture").build();
        Topic serviceDiscovery = Topic.builder().name("Service Discovery").build();
        Topic softPatterns = Topic.builder().name("Software Patterns").build();
        Topic spring = Topic.builder().name("Spring Framework").build();
        Topic springBoot = Topic.builder().name("Spring Boot").build();
        Topic cloud = Topic.builder().name("Cloud Native").build();
        Topic cloudProviders = Topic.builder().name("Cloud Providers").build();

        java.setChildren(Set.of(spring,funcInt));
        spring.setChildren(Set.of(springBoot));
        funcInt.setChildren(Set.of(lambdaExp));
        softPatterns.setChildren(Set.of(funcInt,lambdaExp));
        architecture.setChildren(Set.of(microservices,softPatterns,java));
        microservices.setChildren(Set.of(serviceDiscovery,springBoot));
        cloud.setChildren(Set.of(architecture,microservices,cloudProviders));

        topicRepository.saveAll(List
                .of(lambdaExp,funcInt,java, microservices, architecture, serviceDiscovery, softPatterns, spring, springBoot,cloud, cloudProviders));

        Talk futureOfJava = Talk.builder()
                .name("Future of Java")
                .language(Language.DE)
                .level(Level.ADVANCED)
                .startDateTime(LocalDateTime.of(2020,12,21,10,0))
                .endDateTime(LocalDateTime.of(2020,12,21,11,0))
                .event(javaDays)
                .persons(Set.of(jack, alex))
                .room(vic1)
                .topics(Set.of(java))
                .build();

        Talk javaArchitecture = Talk.builder()
                .name("Java Architecture")
                .language(Language.EN)
                .level(Level.EXPERT)
                .startDateTime(LocalDateTime.of(2020,12,21,12,0))
                .endDateTime(LocalDateTime.of(2020,12,21,15,0))
                .event(javaDays)
                .persons(Set.of(jack, vitali))
                .room(vic2)
                .topics(Set.of(java,spring,architecture))
                .build();

        Talk microWithKotlin = Talk.builder()
                .name("Microservices with Kotlin")
                .language(Language.DE)
                .level(Level.ADVANCED)
                .startDateTime(LocalDateTime.of(2020,12,21,10,0))
                .endDateTime(LocalDateTime.of(2020,12,21,11,0))
                .event(kubeCon)
                .persons(Set.of(hansi, alex))
                .room(zur1)
                .topics(Set.of(cloud,java,microservices))
                .build();

        Talk cloudSecurity = Talk.builder()
                .name("Cloud Security")
                .language(Language.DE)
                .level(Level.ADVANCED)
                .startDateTime(LocalDateTime.of(2020,12,21,10,0))
                .endDateTime(LocalDateTime.of(2020,12,21,11,0))
                .event(kubeCon)
                .persons(Set.of(marc, vitali))
                .topics(Set.of(cloud, cloudProviders,architecture,microservices))
                .room(zur2)
                .build();


        Talk whatsNewAws = Talk.builder()
                .name("AWS new features")
                .language(Language.DE)
                .level(Level.ADVANCED)
                .startDateTime(LocalDateTime.of(2020,12,21,11,0))
                .endDateTime(LocalDateTime.of(2020,12,21,12,0))
                .event(kubeCon)
                .persons(Set.of(marc))
                .room(zur2)
                .topics(Set.of(cloud, cloudProviders))
                .build();



        talkRepository.saveAll(List.of(futureOfJava, javaArchitecture, cloudSecurity, microWithKotlin,whatsNewAws));

    }

    @Override
    public void afterPropertiesSet() throws Exception {

        dropAll();
        create();

    }

}
