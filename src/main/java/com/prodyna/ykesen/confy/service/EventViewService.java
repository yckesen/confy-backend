package com.prodyna.ykesen.confy.service;

import com.google.common.collect.Lists;
import com.prodyna.ykesen.confy.model.Event;
import com.prodyna.ykesen.confy.model.Talk;
import com.prodyna.ykesen.confy.model.Topic;
import com.prodyna.ykesen.confy.model.mapper.EventMapper;
import com.prodyna.ykesen.confy.model.view.EventDTO;
import com.prodyna.ykesen.confy.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class EventViewService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TalkRepository talkRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private EventMapper eventMapper;


    public Iterable<EventDTO> getAllEventsView(){

        Iterable<Event> events = eventRepository.findAll();

        for (Event event: events)
            for (Talk talk : event.getTalks()) {
                talk.setTopics(Set.copyOf(topicRepository.findByTalksId(talk.getId())));
                talk.setRoom(roomRepository.findByTalksId(talk.getId()));
                talk.setPersons(Set.copyOf(personRepository.findByTalksId(talk.getId())));
            }

        Iterable<EventDTO> eventDtos = eventMapper.mapIterableToDTO(events);

        return eventDtos;

    }

}
